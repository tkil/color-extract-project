const color = require("../colors.json");

const colorObjToLines = (colorObj) => {
  return (
    Object.keys(colorObj)
      .sort((a, b) => Number(a) - Number(b))
      // .filter((key) => key !== "name")
      .map((key) => {
        if (key === "name") {
          return `--color-${colorObj.name}: var(--color-${colorObj.name}-500);`;
        }
        return `--color-${colorObj.name}-${key}: ${colorObj[key]};`;
      })
  );
};

const main = () => {
  const lines = color.map((c) => colorObjToLines(c).join("\n")).join("\n\n");
  console.log(lines);
};
main();
