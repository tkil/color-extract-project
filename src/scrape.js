const fetch = require("node-fetch");
const { JSDOM } = require("jsdom");

const getDocument = async () => {
  const url = "https://www.materialui.co/colors";
  const response = await fetch(url);
  const data = await response.text();

  return new JSDOM(data).window.document;
};

const cleanColorName = (name) => name.toLowerCase().replace(" ", "-");

const cleanColorNames = (colorHeaderTupleArray) => colorHeaderTupleArray.map((ct) => [cleanColorName(ct[0]), ct[1]]);

const extractElColorTuple = (el) => el.textContent.split(":").map((s) => s.trim());

const readColorGridHeaderToArray = (rowEl) => {
  const els = rowEl.querySelectorAll(".color_id");
  return Array.from(els).map((el) => extractElColorTuple(el));
};

// const readColorGridTrToArray

const readColorGrid = (document) => {
  const rowEls = Array.from(document.querySelectorAll("tr"));
  let colorHeaderTupleArray = readColorGridHeaderToArray(rowEls[0]);
  colorHeaderTupleArray = cleanColorNames(colorHeaderTupleArray);

  const obj = {};
  for (let r = 1; r <= 10; r++) {
    for (let c = 0; c < colorHeaderTupleArray.length; c++) {
      const colorKey = colorHeaderTupleArray[c][0];
      if (r === 1) {
        obj[colorKey] = { colorName: colorKey };
      }
      let colorRowTuple = readColorGridHeaderToArray(rowEls[r]);
      obj[colorKey][colorRowTuple[c][0]] = colorRowTuple[c][1];
    }
  }
  console.log(obj);
};

const main = async () => {
  const document = await getDocument();
  const colorData = readColorGrid(document);
};

main();
